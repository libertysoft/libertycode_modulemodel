LibertyCode_ModuleModel
=======================



Description
-----------

Library contains modular model components, 
to split and use model components in several modules.

---



Requirement
-----------

- Script language: PHP: version 7 || 8

---



Installation
------------

Several ways are possible:

#### Composer

1. Requirement
    
    It requires composer installation.
    For more information: https://getcomposer.org
    
2. Command: Move in project root path
    
    ```sh
    cd "<project_root_path>"
    ```

3. Command: Installation
    
    ```sh
    php composer.phar require liberty_code/module_model ["<version>"]
    ```
    
4. Note

    - Include vendor
        
        If project uses composer, 
        vendor must be included:
        
        ```php
        require_once('<project_root_path>/vendor/autoload.php');
        ```
    
    - Configuration
    
        Installation command allows to add, 
        on composer file "<project_root_path>/composer.json",
        following configuration:
        
        ```json
        {
            "require": {
                "liberty_code/module_model": "<version>"
            }
        }
        ```

#### Include

1. Download
    
    - Download following repository.
    - Put it on repository root path.
    
2. Include source
    
    ```php
    require_once('<repository_root_path>/include/Include.php');
    ```

---



Usage
-----

#### Entity module

Entity module contains part of configured attributes, 
can be used in configured entities.

_Elements_

- EntityModule

    Allows to design an entity module.
    Contains part of configured attributes, 
    can be used in configured entities.

- ValidatorEntityModule

    Extends entity module features. 
    Can be used in validator configured entities.
    
- SaveEntityModule

    Extends validator entity module features. 
    Can be used in save configured entities.

- EntityModuleCollection

    Allows to design collection of entity modules.
    Uses list of entity modules, 
    to build attributes configuration,
    for specified entity.

- HandleValidatorEntityModule

    Extends validator entity module features. 
    Uses attribute provider, 
    to build and manage its part of configured attributes.
    
- HandleSaveEntityModule

    Extends save entity module features. 
    Uses attribute provider, 
    to build and manage its part of configured attributes.
    
- Builder
    
    Allows to hydrate entity module collection, with entity modules.

- FixBuilder
    
    Extends entity module builder features. 
    Uses fixed array of source data to hydrate entity module collection.
    
_Example_

```php
// Get entity module collection
use liberty_code\module_model\entity_module\model\DefaultEntityModuleCollection;
$entityModuleCollection = new DefaultEntityModuleCollection();
...
// Set entity modules
use liberty_code\module_model\entity_module\model\DefaultEntityModule;
$entityModuleCollection->setTabEntityModule(DefaultEntityModule[...]);
...
// Get entity attribute configuration, from entity modules
$entityModuleCollection->getTabAttributeConfig();
...
```

#### Entity

Module configured entity uses entity module collection, 
to manage its attributes.

_Elements_

- ModuleConfigEntity

    Extends configured entity features. 
    Uses entity module collection, 
    to manage its attributes.

- ModuleValidatorConfigEntity

    Extends validator configured entity features. 
    Uses entity module collection, 
    to manage its attributes.
    
- ModuleSaveConfigEntity

    Extends save configured entity features. 
    Uses entity module collection, 
    to manage its attributes.
    
_Example_

```php
...
// Get new entity
use liberty_code\module_model\entity\model\ModuleConfigEntity
$moduleConfigEntity = new ModuleConfigEntity($entityModuleCollection);
...
if($moduleConfigEntity->checkAttributeValid()) {
    $tabAttributeKey = $moduleConfigEntity->getTabAttributeKey();
    foreach($tabAttributeKey as $attributeKey) {
        echo($moduleConfigEntity->$attributeKey . '<br />');
    }
}
/**
 * Show: 
 * Entity attribute 1 value
 * ...
 * Entity attribute N value
 */
...
```

---



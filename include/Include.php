<?php

// Init var
$strRootPath = dirname(__FILE__) . '/..';

// Include class
include($strRootPath . '/src/entity_module/library/ConstEntityModule.php');
include($strRootPath . '/src/entity_module/library/ToolBoxEntityModule.php');
include($strRootPath . '/src/entity_module/exception/KeyInvalidFormatException.php');
include($strRootPath . '/src/entity_module/exception/CollectionConfigInvalidFormatException.php');
include($strRootPath . '/src/entity_module/exception/CollectionKeyInvalidFormatException.php');
include($strRootPath . '/src/entity_module/exception/CollectionValueInvalidFormatException.php');
include($strRootPath . '/src/entity_module/model/DefaultEntityModule.php');
include($strRootPath . '/src/entity_module/model/ValidatorEntityModule.php');
include($strRootPath . '/src/entity_module/model/SaveEntityModule.php');
include($strRootPath . '/src/entity_module/model/DefaultEntityModuleCollection.php');

include($strRootPath . '/src/entity_module/handle/model/HandleValidatorEntityModule.php');
include($strRootPath . '/src/entity_module/handle/model/HandleSaveEntityModule.php');

include($strRootPath . '/src/entity_module/build/library/ConstBuilder.php');
include($strRootPath . '/src/entity_module/build/exception/DataSrcInvalidFormatException.php');
include($strRootPath . '/src/entity_module/build/model/DefaultBuilder.php');

include($strRootPath . '/src/entity_module/build/fix/library/ConstFixBuilder.php');
include($strRootPath . '/src/entity_module/build/fix/exception/DataSrcInvalidFormatException.php');
include($strRootPath . '/src/entity_module/build/fix/model/FixBuilder.php');

include($strRootPath . '/src/entity/model/ModuleConfigEntity.php');
include($strRootPath . '/src/entity/model/ModuleValidatorConfigEntity.php');

include($strRootPath . '/src/entity/repository/model/ModuleSaveConfigEntity.php');
<?php
/**
 * Description :
 * This class allows to define handle validator entity module.
 * Handle validator entity module is validator entity module, using attribute provider,
 * to build and manage its part of configured attributes.
 *
 * Handle validator entity module allows to configure attributes from following configuration (feature 'getTabConfig'):
 * [
 *     @see ValidatorEntityModule attributes configuration
 * ]
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\module_model\entity_module\handle\model;

use liberty_code\module_model\entity_module\model\ValidatorEntityModule;

use liberty_code\model\entity\model\ConfigEntity;
use liberty_code\model\entity\model\ValidatorConfigEntity;
use liberty_code\handle_model\attribute\provider\api\AttrProviderInterface;



class HandleValidatorEntityModule extends ValidatorEntityModule
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();



    /**
     * Attribute provider object
     * @var AttrProviderInterface
     */
    protected $objAttrProvider;




	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param AttrProviderInterface $objAttrProvider
     */
    public function __construct(
        AttrProviderInterface $objAttrProvider,
        $tabData = array()
    )
    {
        // Set attribute provider
        $this->setAttrProvider($objAttrProvider);

        // Call parent constructor
        parent::__construct($tabData);
    }





    // Methods getters
    // ******************************************************************************

    /**
     * Get attribute provider object.
     *
     * @return AttrProviderInterface
     */
    public function getObjAttrProvider()
    {
        // Return result
        return $this->objAttrProvider;
    }



    /**
     * @inheritdoc
     */
    public function getTabAttributeConfig()
    {
        // Return result
        return $this->objAttrProvider->getTabEntityAttrConfig();
    }



    /**
     * @inheritdoc
     */
    public function getTabAttributeRuleConfig(ValidatorConfigEntity $objEntity = null)
    {
        // Return result
        return $this->objAttrProvider->getTabEntityAttrRuleConfig();
    }



    /**
     * @inheritdoc
     */
    public function getAttributeValueFormatGet($strKey, $value, ConfigEntity $objEntity = null)
    {
        // Return result
        return $this->objAttrProvider->getEntityAttrValueFormatGet($strKey, $value);
    }



    /**
     * @inheritdoc
     */
    public function getAttributeValueFormatSet($strKey, $value, ConfigEntity $objEntity = null)
    {
        // Return result
        return $this->objAttrProvider->getEntityAttrValueFormatSet($strKey, $value);
    }





    // Methods setters
    // ******************************************************************************

    /**
     * Set attribute provider object.
     *
     * @param AttrProviderInterface $objAttrProvider
     */
    public function setAttrProvider(AttrProviderInterface $objAttrProvider)
    {
        // Set data
        $this->objAttrProvider = $objAttrProvider;
    }



}
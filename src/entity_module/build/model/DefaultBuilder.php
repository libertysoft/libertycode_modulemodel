<?php
/**
 * Description :
 * This class allows to define default builder class.
 * Default builder allows to populate default entity module collection,
 * from a specified array of source data.
 *
 * Default builder uses the following specified source data, to hydrate entity module collection:
 * [
 *     "String entity module class path 1",
 *
 *     ...,
 *
 *     "String entity module class path N"
 * ]
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\module_model\entity_module\build\model;

use liberty_code\di\factory\model\DefaultFactory;

use liberty_code\di\provider\api\ProviderInterface;
use liberty_code\module_model\entity_module\model\DefaultEntityModuleCollection;
use liberty_code\module_model\entity_module\build\library\ConstBuilder;
use liberty_code\module_model\entity_module\build\exception\DataSrcInvalidFormatException;



/**
 * @method array getTabDataSrc() get data source array.
 * @method void setTabDataSrc(array $tabDataSrc) Set data source array.
 */
class DefaultBuilder extends DefaultFactory
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param array $tabDataSrc = array()
     */
    public function __construct(ProviderInterface $objProvider = null, array $tabDataSrc = array())
    {
        // Call parent constructor
        parent::__construct($objProvider);

        // Init data source
        $this->setTabDataSrc($tabDataSrc);
    }





    // Methods initialize
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function beanHydrateDefault()
    {
        // Init bean data
        if(!$this->beanExists(ConstBuilder::DATA_KEY_DEFAULT_DATA_SRC))
        {
            $this->__beanTabData[ConstBuilder::DATA_KEY_DEFAULT_DATA_SRC] = array();
        }

        // Call parent method
        parent::beanHydrateDefault();
    }



    /**
     * Hydrate specified entity module collection.
     *
     * @param DefaultEntityModuleCollection $objEntityModuleCollection
     * @param boolean $boolClear = true
     * @throws DataSrcInvalidFormatException
     */
    public function hydrateEntityModuleCollection(
        DefaultEntityModuleCollection $objEntityModuleCollection,
        $boolClear = true
    )
    {
        // Init var
        $boolClear = (is_bool($boolClear) ? $boolClear : true);
        $tabDataSrc = $this->getTabDataSrc();

        // Run each data source
        $tabEntityModule = array();
        foreach($tabDataSrc as $strClassPath)
        {
            // Get new entity module
            $objEntityModule = $this->getObjInstance($strClassPath);

            // Register entity module, if found
            if(!is_null($objEntityModule))
            {
                $tabEntityModule[] = $objEntityModule;
            }
            // Throw exception if entity module not found, from data source
            else
            {
                throw new DataSrcInvalidFormatException(serialize($tabDataSrc));
            }
        }

        // Clear entity modules from collection, if required
        if($boolClear)
        {
            $objEntityModuleCollection->removeEntityModuleAll();
        }

        // Register dependencies on collection
        $objEntityModuleCollection->setTabEntityModule($tabEntityModule);
    }





	// Methods validation
	// ******************************************************************************

	/**
	 * @inheritdoc
	 */
	public function beanCheckValidKey($key, &$error = null)
	{
		// Init var
		$tabKey = array(
			ConstBuilder::DATA_KEY_DEFAULT_DATA_SRC
		);
        $result =
            in_array($key, $tabKey) ||
            parent::beanCheckValidKey($key, $error);

		// Return result
		return $result;
	}



	/**
	 * @inheritdoc
	 */
	public function beanCheckValidValue($key, $value, &$error = null)
	{
        // Init var
        $result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstBuilder::DATA_KEY_DEFAULT_DATA_SRC:
                    DataSrcInvalidFormatException::setCheck($value);
                    break;

                default:
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
	}
	
	
	
}
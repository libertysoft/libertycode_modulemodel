<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\module_model\entity_module\build\fix\library;



class ConstFixBuilder
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Exception message constants
    const EXCEPT_MSG_DATA_SRC_INVALID_FORMAT =
        'Following data source "%1$s" invalid! 
        The data source must be an array and following the fixed entity module builder data source standard.';
}
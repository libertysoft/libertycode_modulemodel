<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\module_model\entity_module\build\fix\exception;

use liberty_code\module_model\entity_module\build\fix\library\ConstFixBuilder;



class DataSrcInvalidFormatException extends \Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
     * 
	 * @param mixed $dataSrc
     */
	public function __construct($dataSrc)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf
        (
            ConstFixBuilder::EXCEPT_MSG_DATA_SRC_INVALID_FORMAT,
            mb_strimwidth(strval($dataSrc), 0, 50, "...")
        );
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************

	/**
	 * Check if specified data source has valid format.
	 * 
     * @param mixed $dataSrc
     * @param array $tabFixDataSrc
	 * @return boolean
	 * @throws static
     */
	static public function setCheck($dataSrc, array $tabFixDataSrc)
    {
		// Init var
		$result =
            // Check valid array
            is_array($dataSrc) &&
            ($tabFixDataSrc === $dataSrc);

		// Throw exception if check not pass
		if(!$result)
		{
			throw new static((is_array($dataSrc) ? serialize($dataSrc) : $dataSrc));
		}
		
		// Return result
		return $result;
    }
	
	
	
}
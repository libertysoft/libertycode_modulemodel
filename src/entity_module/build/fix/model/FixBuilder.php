<?php
/**
 * Description :
 * This class allows to define fixed builder class.
 * Fixed builder allows to populate default entity module collection,
 * from a fixed array of source data.
 *
 * Fixed builder uses the following specified source data, to hydrate entity module collection:
 * [
 *     Default builder source data
 * ]
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\module_model\entity_module\build\fix\model;

use liberty_code\module_model\entity_module\build\model\DefaultBuilder;

use liberty_code\di\provider\api\ProviderInterface;
use liberty_code\module_model\entity_module\build\library\ConstBuilder;
use liberty_code\module_model\entity_module\build\fix\exception\DataSrcInvalidFormatException;



abstract class FixBuilder extends DefaultBuilder
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function __construct(ProviderInterface $objProvider = null)
    {
        // Init var
        $tabDataSrc = $this->getTabFixDataSrc();

        // Call parent constructor
        parent::__construct($objProvider, $tabDataSrc);
    }





    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        // $result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstBuilder::DATA_KEY_DEFAULT_DATA_SRC:
                    $tabDataSrc = $this->getTabFixDataSrc();
                    DataSrcInvalidFormatException::setCheck($value, $tabDataSrc);
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;

                default:
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * Get fixed data source array.
     *
     * @return array
     */
    abstract protected function getTabFixDataSrc();
	
	
	
}
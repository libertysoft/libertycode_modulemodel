<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\module_model\entity_module\build\library;



class ConstBuilder
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Properties
    const DATA_KEY_DEFAULT_DATA_SRC = 'tabDataSrc';



    // Exception message
    const EXCEPT_MSG_DATA_SRC_INVALID_FORMAT =
        'Following data source "%1$s" invalid! 
        The data source must be an array and following the default entity module builder data source standard.';
}
<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\module_model\entity_module\exception;

use liberty_code\module_model\entity_module\library\ConstEntityModule;



class CollectionConfigInvalidFormatException extends \Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
     * 
	 * @param mixed $config
     */
	public function __construct($config)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf
        (
            ConstEntityModule::EXCEPT_MSG_COLLECTION_CONFIG_INVALID_FORMAT,
            mb_strimwidth(strval($config), 0, 10, "...")
        );
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************

    /**
     * Check if specified config has valid format.
     *
     * @param mixed $config
     * @return boolean
     */
    protected static function checkConfigIsValid($config)
    {
        // Init var
        $result =
            // Check valid cache required option
            (
                (!isset($config[ConstEntityModule::TAB_COLLECTION_CONFIG_KEY_CACHE_REQUIRE])) ||
                (
                    // Check is boolean or numeric (0 = false, 1 = true)
                    is_bool($config[ConstEntityModule::TAB_COLLECTION_CONFIG_KEY_CACHE_REQUIRE]) ||
                    is_int($config[ConstEntityModule::TAB_COLLECTION_CONFIG_KEY_CACHE_REQUIRE]) ||
                    (
                        is_string($config[ConstEntityModule::TAB_COLLECTION_CONFIG_KEY_CACHE_REQUIRE]) &&
                        ctype_digit($config[ConstEntityModule::TAB_COLLECTION_CONFIG_KEY_CACHE_REQUIRE])
                    )
                )
            ) &&

            // Check valid overwrite required option
            (
                (!isset($config[ConstEntityModule::TAB_COLLECTION_CONFIG_KEY_OVERWRITE_REQUIRE])) ||
                (
                    // Check is boolean or numeric (0 = false, 1 = true)
                    is_bool($config[ConstEntityModule::TAB_COLLECTION_CONFIG_KEY_OVERWRITE_REQUIRE]) ||
                    is_int($config[ConstEntityModule::TAB_COLLECTION_CONFIG_KEY_OVERWRITE_REQUIRE]) ||
                    (
                        is_string($config[ConstEntityModule::TAB_COLLECTION_CONFIG_KEY_OVERWRITE_REQUIRE]) &&
                        ctype_digit($config[ConstEntityModule::TAB_COLLECTION_CONFIG_KEY_OVERWRITE_REQUIRE])
                    )
                )
            );

        // Return result
        return $result;
    }



    /**
     * Check if specified config has valid format
     *
     * @param mixed $config
     * @return boolean
     * @throws static
     */
    static public function setCheck($config)
    {
        // Init var
        $result =
            // Check valid array
            is_array($config) &&

            // Check valid config
            static::checkConfigIsValid($config);

        // Throw exception if check not pass
        if(!$result)
        {
            throw new static((is_array($config) ? serialize($config) : $config));
        }

        // Return result
        return $result;
    }
	
	
	
}
<?php
/**
 * Description :
 * This class allows to define default entity module collection class.
 * key => entity module.
 * Can be consider is base of all entity module collection types.
 *
 * Default entity module collection uses the following specified configuration:
 * [
 *     cache_require(optional: got true if not found): true / false,
 *
 *     overwrite_require(optional: got true if not found): true / false
 * ]
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\module_model\entity_module\model;

use liberty_code\library\bean\model\DefaultBean;

use liberty_code\library\bean\library\ConstBean;
use liberty_code\model\entity\model\ConfigEntity;
use liberty_code\model\entity\model\ValidatorConfigEntity;
use liberty_code\model\entity\repository\model\SaveConfigEntity;
use liberty_code\module_model\entity_module\library\ConstEntityModule;
use liberty_code\module_model\entity_module\library\ToolBoxEntityModule;
use liberty_code\module_model\entity_module\model\DefaultEntityModule;
use liberty_code\module_model\entity_module\exception\CollectionConfigInvalidFormatException;
use liberty_code\module_model\entity_module\exception\CollectionKeyInvalidFormatException;
use liberty_code\module_model\entity_module\exception\CollectionValueInvalidFormatException;



class DefaultEntityModuleCollection extends DefaultBean
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /**
     * Init instances table to dissociate this class from parent
     * @var array
     */
    static protected $__instanceTab = array();



    /** @var null|ConfigEntity */
    protected $objEntity;



    /** @var array */
    protected $tabConfig;



    /** @var array */
    protected $tabCacheAttribute;





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param ConfigEntity $objEntity = null,
     * @param array $tabConfig = null
     */
    public function __construct(
        ConfigEntity $objEntity = null,
        array $tabConfig = null,
        array $tabData = array())
    {
        // Init var
        $this->objEntity = null;
        $this->tabConfig = array();
        $this->tabCacheAttribute = array();

        // Call parent constructor
        parent::__construct($tabData);

        // Init entity, if required
        if(!is_null($objEntity))
        {
            $this->setEntity($objEntity);
        }

        // Init configuration, if required
        if(!is_null($tabConfig))
        {
            $this->setConfig($tabConfig);
        }
    }





    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        $result = true;

        // Validation
        try
        {
            // Check value argument
            CollectionValueInvalidFormatException::setCheck($value);

            // Check key argument
            /** @var DefaultEntityModule $value */
            if(
                (!is_string($key)) ||
                ($key != $value->getStrKey())
            )
            {
                throw new CollectionKeyInvalidFormatException($key);
            }

            // Remove cache
            $this->removeCache();
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }





    // Methods check
    // ******************************************************************************

    /**
     * Check cache required.
     *
     * @return boolean
     */
    protected function checkCacheRequired()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = (
            (!array_key_exists(ConstEntityModule::TAB_COLLECTION_CONFIG_KEY_CACHE_REQUIRE, $tabConfig)) ||
            (intval($tabConfig[ConstEntityModule::TAB_COLLECTION_CONFIG_KEY_CACHE_REQUIRE]) != 0)
        );

        // Return result
        return $result;
    }



    /**
     * Check overwrite required.
     *
     * @return boolean
     */
    protected function checkOverwriteRequired()
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $result = (
            (!array_key_exists(ConstEntityModule::TAB_COLLECTION_CONFIG_KEY_OVERWRITE_REQUIRE, $tabConfig)) ||
            (intval($tabConfig[ConstEntityModule::TAB_COLLECTION_CONFIG_KEY_OVERWRITE_REQUIRE]) != 0)
        );

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * Get entity object.
     *
     * @return null|ConfigEntity
     */
    public function getObjEntity()
    {
        // Return result
        return $this->objEntity;
    }



    /**
     * Get configuration array.
     *
     * @return array
     */
    public function getTabConfig()
    {
        // Return result
        return $this->tabConfig;
    }



    /**
     * Get entity module from specified key.
     *
     * @param string $strKey
     * @return null|DefaultEntityModule
     */
    public function getObjEntityModule($strKey)
    {
        // Init var
        $result = null;

        // Try to get entity module object if found
        try
        {
            if($this->beanDataExists($strKey))
            {
                $result = $this->beanGetData($strKey);
            }
        }
        catch(\Exception $e)
        {
        }

        // Return result
        return $result;
    }





    // Methods setters
    // ******************************************************************************

    /**
     * Set entity object.
     *
     * @param ConfigEntity $objEntity
     */
    public function setEntity(ConfigEntity $objEntity)
    {
        $this->objEntity = $objEntity;

        // Remove cache
        $this->removeCache();
    }



    /**
     * Set configuration array.
     *
     * @param array $tabConfig
     * @throws CollectionConfigInvalidFormatException
     */
    public function setConfig(array $tabConfig)
    {
        // Set check argument
        CollectionConfigInvalidFormatException::setCheck($tabConfig);

        $this->tabConfig = $tabConfig;

        // Remove cache
        $this->removeCache();
    }



    /**
     * Remove cache.
     *
     * @param string $strCacheKey = null
     */
    public function removeCache($strCacheKey = null)
    {
        // Remove specific cache, if required
        if(
            is_string($strCacheKey) &&
            isset($this->tabCacheAttribute[$strCacheKey])
        )
        {
            unset($this->tabCacheAttribute[$strCacheKey]);
        }
        // Remove all cache, else
        else
        {
            $this->tabCacheAttribute = array();
        }
    }



    /**
     * Set entity module and return its key.
     *
     * @param DefaultEntityModule $objEntityModule
     * @return string
     */
    public function setEntityModule(DefaultEntityModule $objEntityModule)
    {
        // Init var
        $strKey = $objEntityModule->getStrKey();

        // Register instance
        $this->beanPutData($strKey, $objEntityModule);

        // return result
        return $strKey;
    }



    /**
     * Set list of entity modules (index array or collection) and
     * return its list of keys (index array).
     *
     * @param array|static $tabEntityModule
     * @return array
     */
    public function setTabEntityModule($tabEntityModule)
    {
        // Init var
        $result = array();

        // Case index array of entity modules
        if(is_array($tabEntityModule))
        {
            // Run all entity modules and for each, try to set
            foreach($tabEntityModule as $entityModule)
            {
                $strKey = $this->setEntityModule($entityModule);
                $result[] = $strKey;
            }
        }
        // Case collection of entity modules
        else if($tabEntityModule instanceof static)
        {
            // Run all entity modules and for each, try to set
            $tabKey = $this->beanGetTabData(ConstBean::OPTION_TABLE_DATA_KEY);
            foreach($tabKey as $strKey)
            {
                $objEntityModule = $tabEntityModule->getObjEntityModule($strKey);
                $strKey = $this->setEntityModule($objEntityModule);
                $result[] = $strKey;
            }
        }

        // return result
        return $result;
    }



    /**
     * Remove entity module and return its instance.
     *
     * @param string $strKey
     * @return DefaultEntityModule
     */
    public function removeEntityModule($strKey)
    {
        // Init var
        $result = $this->getObjEntityModule($strKey);

        // Remove item
        $this->beanRemoveData($strKey);

        // return result
        return $result;
    }



    /**
     * Remove all entity modules.
     */
    public function removeEntityModuleAll()
    {
        // Ini var
        $tabKey = $this->beanGetTabData(ConstBean::OPTION_TABLE_DATA_KEY);

        foreach($tabKey as $strKey)
        {
            $this->removeEntityModule($strKey);
        }
    }





    // Methods module attribute
    // ******************************************************************************

    /**
     * Check if specified module attribute is accessible.
     *
     * @param string|DefaultEntityModule $entityModule
     * @param string $strKey
     * @return boolean
     */
    public function checkAttributeAccess($entityModule, $strKey)
    {
        // Init var
        $objEntityModule = (
            ($entityModule instanceof DefaultEntityModule) ?
                $entityModule :
                $this->getObjEntityModule($entityModule)
        );
        $strEntityModuleKey = $objEntityModule->getStrKey();
        $objEntity = $this->getObjEntity();
        $strCacheKey = ConstEntityModule::COLLECTION_CACHE_KEY_ATTRIBUTE_ACCESS;
        $boolCache = $this->checkCacheRequired();

        // Init cache, if required
        $this->tabCacheAttribute[$strCacheKey] = (
            (
                isset($this->tabCacheAttribute[$strCacheKey]) &&
                is_array($this->tabCacheAttribute[$strCacheKey])
            ) ?
                $this->tabCacheAttribute[$strCacheKey] :
                array()
        );
        $this->tabCacheAttribute[$strCacheKey][$strEntityModuleKey] = (
            (
                isset($this->tabCacheAttribute[$strCacheKey][$strEntityModuleKey]) &&
                is_array($this->tabCacheAttribute[$strCacheKey][$strEntityModuleKey])
            ) ?
                $this->tabCacheAttribute[$strCacheKey][$strEntityModuleKey] :
                array()
        );

        // Get result
        $result = (
            (
                $boolCache &&
                is_string($strEntityModuleKey) &&
                is_string($strKey) &&
                isset($this->tabCacheAttribute[$strCacheKey][$strEntityModuleKey][$strKey]) &&
                is_bool($this->tabCacheAttribute[$strCacheKey][$strEntityModuleKey][$strKey])
            ) ?
                // Get from cache, if required
                $result = $this->tabCacheAttribute[$strCacheKey][$strEntityModuleKey][$strKey] :
                // Else calculate
                $objEntityModule->checkAttributeAccess($strKey, $objEntity)
        );

        // Set result on cache, if required
        if($boolCache)
        {
            $this->tabCacheAttribute[$strCacheKey][$strEntityModuleKey][$strKey] = $result;
        }

        // Return result
        return $result;
    }



    /**
     * Check if specified module attribute has valid value.
     * Associative array of errors can be provided.
     *
     * Associative array of errors format:
     * @see ToolBoxEntityModule::checkAttributeValueValid().
     *
     * @param string $strKey
     * @param mixed $value
     * @param array &$tabError = array()
     * @return boolean
     */
    public function checkAttributeValueValid($strKey, $value, array &$tabError = array())
    {
        // Init var
        $tabEntityModule = $this->beanGetTabData(ConstBean::OPTION_TABLE_DATA_KEY_VALUE);
        $objEntity = $this->getObjEntity();
        $boolOverwrite = $this->checkOverwriteRequired();

        // Return result
        return ToolBoxEntityModule::checkAttributeValueValid(
            $tabEntityModule,
            $strKey,
            $value,
            $tabError,
            $objEntity,
            array($this, 'checkAttributeAccess'),
            $boolOverwrite
        );
    }



    /**
     * Get index array of module attribute configuration.
     *
     * @return array
     */
    public function getTabAttributeConfig()
    {
        // Init var
        $tabEntityModule = $this->beanGetTabData(ConstBean::OPTION_TABLE_DATA_KEY_VALUE);
        $objEntity = $this->getObjEntity();
        $boolOverwrite = $this->checkOverwriteRequired();
        $strCacheKey = ConstEntityModule::COLLECTION_CACHE_KEY_ATTRIBUTE_CONFIG;
        $boolCache = $this->checkCacheRequired();
        $result = (
            (
                $boolCache &&
                isset($this->tabCacheAttribute[$strCacheKey]) &&
                is_array($this->tabCacheAttribute[$strCacheKey])
            ) ?
                // Get from cache, if required
                $result = $this->tabCacheAttribute[$strCacheKey] :
                // Else calculate
                $result = ToolBoxEntityModule::getTabAttributeConfig(
                    $tabEntityModule,
                    $objEntity,
                    array($this, 'checkAttributeAccess'),
                    $boolOverwrite
                )
        );

        // Set result on cache, if required
        if($boolCache)
        {
            $this->tabCacheAttribute[$strCacheKey] = $result;
        }

        // Return result
        return $result;
    }



    /**
     * Get module attribute rule configurations array.
     *
     * @return array
     */
    public function getTabAttributeRuleConfig()
    {
        // Init var
        $tabEntityModule = $this->beanGetTabData(ConstBean::OPTION_TABLE_DATA_KEY_VALUE);
        $objEntity = $this->getObjEntity();
        $objEntity = (($objEntity instanceof ValidatorConfigEntity) ? $objEntity : null);
        $boolOverwrite = $this->checkOverwriteRequired();
        $strCacheKey = ConstEntityModule::COLLECTION_CACHE_KEY_ATTRIBUTE_RULE_CONFIG;
        $boolCache = $this->checkCacheRequired();
        $result = (
            (
                $boolCache &&
                isset($this->tabCacheAttribute[$strCacheKey]) &&
                is_array($this->tabCacheAttribute[$strCacheKey])
            ) ?
                // Get from cache, if required
                $result = $this->tabCacheAttribute[$strCacheKey] :
                // Else calculate
                $result = ToolBoxEntityModule::getTabAttributeRuleConfig(
                    $tabEntityModule,
                    $objEntity,
                    array($this, 'checkAttributeAccess'),
                    $boolOverwrite
                )
        );

        // Set result on cache, if required
        if($boolCache)
        {
            $this->tabCacheAttribute[$strCacheKey] = $result;
        }

        // Return result
        return $result;
    }



    /**
     * Get specified module attribute formatted value when get action required.
     *
     * @param string $strKey
     * @param mixed $value
     * @return mixed
     */
    public function getAttributeValueFormatGet($strKey, $value)
    {
        // Init var
        $tabEntityModule = $this->beanGetTabData(ConstBean::OPTION_TABLE_DATA_KEY_VALUE);
        $objEntity = $this->getObjEntity();
        $boolOverwrite = $this->checkOverwriteRequired();

        // Return result
        return ToolBoxEntityModule::getAttributeValueFormatGet(
            $tabEntityModule,
            $strKey,
            $value,
            $objEntity,
            array($this, 'checkAttributeAccess'),
            $boolOverwrite
        );
    }



    /**
     * Get specified module attribute formatted value when set action required.
     *
     * @param string $strKey
     * @param mixed $value
     * @return mixed
     */
    public function getAttributeValueFormatSet($strKey, $value)
    {
        // Init var
        $tabEntityModule = $this->beanGetTabData(ConstBean::OPTION_TABLE_DATA_KEY_VALUE);
        $objEntity = $this->getObjEntity();
        $boolOverwrite = $this->checkOverwriteRequired();

        // Return result
        return ToolBoxEntityModule::getAttributeValueFormatSet(
            $tabEntityModule,
            $strKey,
            $value,
            $objEntity,
            array($this, 'checkAttributeAccess'),
            $boolOverwrite
        );
    }



    /**
     * Get specified module attribute formatted value when get action required, to be saved.
     *
     * @param string $strKey
     * @param mixed $value
     * @return mixed
     */
    public function getAttributeValueSaveFormatGet($strKey, $value)
    {
        // Init var
        $tabEntityModule = $this->beanGetTabData(ConstBean::OPTION_TABLE_DATA_KEY_VALUE);
        $objEntity = $this->getObjEntity();
        $objEntity = (($objEntity instanceof SaveConfigEntity) ? $objEntity : null);
        $boolOverwrite = $this->checkOverwriteRequired();

        // Return result
        return ToolBoxEntityModule::getAttributeValueSaveFormatGet(
            $tabEntityModule,
            $strKey,
            $value,
            $objEntity,
            array($this, 'checkAttributeAccess'),
            $boolOverwrite
        );
    }



    /**
     * Get specified module attribute formatted value when set action required, to be load.
     *
     * @param string $strKey
     * @param mixed $value
     * @return mixed
     */
    public function getAttributeValueSaveFormatSet($strKey, $value)
    {
        // Init var
        $tabEntityModule = $this->beanGetTabData(ConstBean::OPTION_TABLE_DATA_KEY_VALUE);
        $objEntity = $this->getObjEntity();
        $objEntity = (($objEntity instanceof SaveConfigEntity) ? $objEntity : null);
        $boolOverwrite = $this->checkOverwriteRequired();

        // Return result
        return ToolBoxEntityModule::getAttributeValueSaveFormatSet(
            $tabEntityModule,
            $strKey,
            $value,
            $objEntity,
            array($this, 'checkAttributeAccess'),
            $boolOverwrite
        );
    }



}
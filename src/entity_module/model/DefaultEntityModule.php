<?php
/**
 * Description :
 * This class allows to define default entity module.
 * Default entity module contains part of configured attributes,
 * can be used in configured entity.
 *
 * Default entity module allows to configure attributes from following configuration (feature 'getTabConfig'):
 * [
 *     @see ConfigEntity attributes configuration
 * ]
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\module_model\entity_module\model;

use liberty_code\library\bean\model\FixBean;

use liberty_code\model\entity\model\ConfigEntity;
use liberty_code\model\entity\exception\ConfigInvalidFormatException;
use liberty_code\module_model\entity_module\library\ConstEntityModule;
use liberty_code\module_model\entity_module\library\ToolBoxEntityModule;
use liberty_code\module_model\entity_module\exception\KeyInvalidFormatException;



abstract class DefaultEntityModule extends FixBean
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();



    /**
     * Attribute configurations
     * @var array
     */
    protected $tabAttributeConfig;




	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function __construct($tabData = array())
    {
        // Init var
        $this->tabAttributeConfig = array();

        // Call parent constructor
        parent::__construct($tabData);
    }





    // Methods initialize
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function beanHydrateDefault()
    {
        // Set module attribute configuration
        $this->hydrateTabAttributeConfig();
    }



    /**
     * Hydrate module attribute configuration array,
     * from configuration specified in feature 'getTabConfig'.
     *
     * @throws ConfigInvalidFormatException
     */
    protected function hydrateTabAttributeConfig()
    {
        // Init var
        $this->tabAttributeConfig = array();
        $tabConfig = $this->getTabAttributeConfig();

        // Set check configuration
        ConfigInvalidFormatException::setCheck($tabConfig);

        // Run all module attribute configurations
        foreach($tabConfig as $config)
        {
            // Get info
            $strKey = ToolBoxEntityModule::getStrAttributeKey($config);

            // Register module attribute configuration
            $this->tabAttributeConfig[$strKey] = $config;
        }
    }





    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidRemove($key, &$error = null)
    {
        // Return result
        return false;
    }





    // Methods check
    // ******************************************************************************

    /**
     * Check if specified module attribute exists.
     *
     * @param string $strKey
     * @return boolean
     * @throws KeyInvalidFormatException
     */
    public function checkAttributeExists($strKey)
    {
        // Set check argument
        KeyInvalidFormatException::setCheck($strKey);

        // Return result
        return array_key_exists($strKey, $this->tabAttributeConfig);
    }



    /**
     * Check if specified module attribute is accessible.
     * Overwrite it to implement specific check.
     *
     * @param string $strKey
     * @param ConfigEntity $objEntity = null
     * @return boolean
     */
    public function checkAttributeAccess($strKey, ConfigEntity $objEntity = null)
    {
        // Return result
        return $this->checkAttributeExists($strKey);
    }



    /**
     * Check if specified module attribute has valid value
     * (@see ConfigEntity::checkAttributeValueValid() ).
     * Overwrite it to implement specific check.
     *
     * @param string $strKey
     * @param mixed $value
     * @param array &$tabError = array()
     * @param ConfigEntity $objEntity = null
     * @return boolean
     * @throws KeyInvalidFormatException
     */
    public function checkAttributeValueValid($strKey, $value, array &$tabError = array(), ConfigEntity $objEntity = null)
    {
        // Set check argument
        KeyInvalidFormatException::setCheck($strKey);

        // Check by attribute
        switch($strKey)
        {
            default:
                $result = true;
                $tabError = array();
                break;
        }

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * Get string default key.
     *
     * @return string
     */
    protected function getStrKeyDefault()
    {
        // Return result
        return sprintf(
            ConstEntityModule::CONF_DEFAULT_ENTITY_MODULE_KEY_PATTERN_DECORATION,
            spl_object_hash($this)
        );
    }



    /**
     * Get string key (considered as entity module id).
     *
     * @return string
     */
    public function getStrKey()
    {
        // Return result
        return $this->getStrKeyDefault();
    }



    /**
     * Get index array of module attribute configuration
     * (@see ConfigEntity::getTabConfig() ).
     *
     * @return array
     */
    abstract public function getTabAttributeConfig();



    /**
     * Get specified module attribute formatted value when get action required
     * (@see ConfigEntity::getAttributeValueFormatGet() ).
     * Overwrite it to implement specific format.
     *
     * @param string $strKey
     * @param mixed $value
     * @param ConfigEntity $objEntity = null
     * @return mixed
     * @throws KeyInvalidFormatException
     */
    public function getAttributeValueFormatGet($strKey, $value, ConfigEntity $objEntity = null)
    {
        // Set check argument
        KeyInvalidFormatException::setCheck($strKey);

        // Format by attribute
        switch($strKey)
        {
            default:
                $result = $value;
                break;
        }

        // Return result
        return $result;
    }



    /**
     * Get specified module attribute formatted value when set action required
     * (@see ConfigEntity::getAttributeValueFormatSet() ).
     * Overwrite it to implement specific format.
     *
     * @param string $strKey
     * @param mixed $value
     * @param ConfigEntity $objEntity = null
     * @return mixed
     * @throws KeyInvalidFormatException
     */
    public function getAttributeValueFormatSet($strKey, $value, ConfigEntity $objEntity = null)
    {
        // Set check argument
        KeyInvalidFormatException::setCheck($strKey);

        // Format by attribute
        switch($strKey)
        {
            default:
                $result = $value;
                break;
        }

        // Return result
        return $result;
    }



}
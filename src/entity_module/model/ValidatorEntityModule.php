<?php
/**
 * Description :
 * This class allows to define validator entity module.
 * Validator entity module contains part of configured attributes,
 * can be used in validator configured entity.
 *
 * Validator entity module allows to configure attributes from following configuration (feature 'getTabConfig'):
 * [
 *     @see ValidatorEntity attributes configuration
 * ]
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\module_model\entity_module\model;

use liberty_code\module_model\entity_module\model\DefaultEntityModule;

use liberty_code\model\entity\model\ValidatorConfigEntity;



abstract class ValidatorEntityModule extends DefaultEntityModule
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();




	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * Get module attribute rule configurations array
     * (@see ValidatorConfigEntity::getTabRuleConfig() ).
     * Overwrite it to implement specific rule configuration.
     *
     * @param ValidatorConfigEntity $objEntity = null
     * @return array
     */
    public function getTabAttributeRuleConfig(ValidatorConfigEntity $objEntity = null)
    {
        // Return result
        return array();
    }



}
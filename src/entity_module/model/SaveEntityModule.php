<?php
/**
 * Description :
 * This class allows to define save entity module.
 * Save entity module contains part of configured attributes,
 * can be used in save configured entity.
 *
 * Save entity module allows to configure attributes from following configuration (feature 'getTabConfig'):
 * [
 *     @see DefaultSaveEntity attributes configuration
 * ]
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\module_model\entity_module\model;

use liberty_code\module_model\entity_module\model\ValidatorEntityModule;

use liberty_code\model\entity\repository\model\SaveConfigEntity;
use liberty_code\module_model\entity_module\exception\KeyInvalidFormatException;



abstract class SaveEntityModule extends ValidatorEntityModule
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();




	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * Get specified module attribute formatted value when get action required, to be saved
     * (@see SaveConfigEntity::getAttributeValueSaveFormatGet() ).
     * Overwrite it to implement specific format.
     *
     * @param string $strKey
     * @param mixed $value
     * @param SaveConfigEntity $objEntity = null
     * @return mixed
     * @throws KeyInvalidFormatException
     */
    public function getAttributeValueSaveFormatGet($strKey, $value, SaveConfigEntity $objEntity = null)
    {
        // Set check argument
        KeyInvalidFormatException::setCheck($strKey);

        // Format by attribute
        switch($strKey)
        {
            default:
                $result = $value;
                break;
        }

        // Return result
        return $result;
    }



    /**
     * Get specified module attribute formatted value when set action required, to be loaded
     * (@see SaveConfigEntity::getAttributeValueSaveFormatSet() ).
     * Overwrite it to implement specific format.
     *
     * @param string $strKey
     * @param mixed $value
     * @param SaveConfigEntity $objEntity = null
     * @return mixed
     * @throws KeyInvalidFormatException
     */
    public function getAttributeValueSaveFormatSet($strKey, $value, SaveConfigEntity $objEntity = null)
    {
        // Set check argument
        KeyInvalidFormatException::setCheck($strKey);

        // Format by attribute
        switch($strKey)
        {
            default:
                $result = $value;
                break;
        }

        // Return result
        return $result;
    }



}
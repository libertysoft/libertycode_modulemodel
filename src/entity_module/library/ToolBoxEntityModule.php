<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\module_model\entity_module\library;

use liberty_code\library\instance\model\Multiton;

use liberty_code\model\entity\library\ConstEntity;
use liberty_code\model\entity\model\ConfigEntity;
use liberty_code\model\entity\model\ValidatorConfigEntity;
use liberty_code\model\entity\repository\model\SaveConfigEntity;
use liberty_code\module_model\entity_module\model\DefaultEntityModule;
use liberty_code\module_model\entity_module\model\ValidatorEntityModule;
use liberty_code\module_model\entity_module\model\SaveEntityModule;



class ToolBoxEntityModule extends Multiton
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /**
     * Init instances table to dissociate this class from parent
     * @var array
     */
    static protected $__instanceTab = array();



    /**
     * Only 1 instance authorized (Singleton)
     * @var int
     */
    static protected $__instanceIntCountLimit = 1;





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Methods check
    // ******************************************************************************

    /**
     * Check if specified module attribute has valid value,
     * from specified index array of entity module objects.
     * Associative array of errors can be provided.
     *
     * Associative array of errors format:
     * @see DefaultEntityModule::checkAttributeValueValid() .
     *
     * Check attribute access callable format:
     * boolean function(DefaultEntityModule $objEntityModule, string $strKey, EntityInterface $objEntity = null).
     *
     * @param array $tabEntityModule
     * @param string $strKey
     * @param mixed $value
     * @param array &$tabError = array(),
     * @param ConfigEntity $objEntity = null
     * @param callable $callCheckAttributeAccess = null
     * @param boolean $boolOverwrite = true
     * @return boolean
     */
    public static function checkAttributeValueValid(
        array $tabEntityModule,
        $strKey,
        $value,
        array &$tabError = array(),
        ConfigEntity $objEntity = null,
        $callCheckAttributeAccess = null,
        $boolOverwrite = true
    )
    {
        // Init var
        $result = true;
        $tabEntityModule = array_values($tabEntityModule);
        $callCheckAttributeAccess = (is_callable($callCheckAttributeAccess) ? $callCheckAttributeAccess : null);
        $boolOverwrite = (is_bool($boolOverwrite) ? $boolOverwrite : true);

        // Run each entity module
        $boolContinue = true;
        for($intCpt = 0; ($intCpt < count($tabEntityModule)) && $boolContinue; $intCpt++)
        {
            // Get info
            $objEntityModule = $tabEntityModule[$intCpt];

            // Check entity module eligible
            if(
                ($objEntityModule instanceof DefaultEntityModule) &&
                ($objEntityModule->checkAttributeExists($strKey)) &&
                (
                    (
                        (!is_null($callCheckAttributeAccess)) &&
                        $callCheckAttributeAccess($objEntityModule, $strKey, $objEntity)
                    ) ||
                    (
                        is_null($callCheckAttributeAccess) &&
                        $objEntityModule->checkAttributeAccess($strKey, $objEntity)
                    )
                )
            )
            {
                // Check attribute has valid value
                $result = $objEntityModule->checkAttributeValueValid($strKey, $value, $tabError, $objEntity);
                $boolContinue = $boolOverwrite;
            }
        }

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * Get module attribute key,
     * from specified module attribute configuration.
     *
     * @param mixed $config
     * @return null|string
     */
    public static function getStrAttributeKey($config)
    {
        // Init var
        $result = (
        is_string($config) ?
            $config :
            (
            (
                isset($config[ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY]) &&
                is_string($config[ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY])
            ) ?
                $config[ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY] :
                null
            )

        );

        // Return result
        return $result;
    }



    /**
     * Get index array of module attribute configuration,
     * from specified index array of entity module objects.
     *
     * Check attribute access callable format:
     * boolean function(DefaultEntityModule $objEntityModule, string $strKey, EntityInterface $objEntity = null).
     *
     * @param array $tabEntityModule
     * @param ConfigEntity $objEntity = null,
     * @param callable $callCheckAttributeAccess = null
     * @param boolean $boolOverwrite = true
     * @return array
     */
    public static function getTabAttributeConfig(
        array $tabEntityModule,
        ConfigEntity $objEntity = null,
        $callCheckAttributeAccess = null,
        $boolOverwrite = true
    )
    {
        // Init var
        $result = array();
        $tabEntityModule = array_values($tabEntityModule);
        $callCheckAttributeAccess = (is_callable($callCheckAttributeAccess) ? $callCheckAttributeAccess : null);
        $boolOverwrite = (is_bool($boolOverwrite) ? $boolOverwrite : true);

        // Run each entity module
        foreach($tabEntityModule as $objEntityModule)
        {
            // Check entity module eligible
            if($objEntityModule instanceof DefaultEntityModule)
            {
                // Get index array of module attribute configuration
                $tabConfig = $objEntityModule->getTabAttributeConfig();

                // Run each module attribute configuration
                foreach($tabConfig as $config)
                {
                    $strAttributeKey = static::getStrAttributeKey($config);
                    $boolFind = false;
                    for($cpt = 0; ($cpt < count($result)) && (!$boolFind); $cpt++)
                    {
                        $resultConfig = $result[$cpt];
                        $strResultAttributeKey = static::getStrAttributeKey($resultConfig);

                        $boolFind = ($strAttributeKey == $strResultAttributeKey);
                    }

                    // Register module attribute configuration, if required
                    if(
                        (
                            (!is_null($callCheckAttributeAccess)) &&
                            $callCheckAttributeAccess($objEntityModule, $strAttributeKey, $objEntity)
                        ) ||
                        (
                            is_null($callCheckAttributeAccess) &&
                            $objEntityModule->checkAttributeAccess($strAttributeKey, $objEntity)
                        )
                    )
                    {
                        if($boolFind)
                        {
                            $cpt--;
                            if($boolOverwrite)
                            {
                                $result[$cpt] = $config;
                            }
                        }
                        else
                        {
                            $result[] = $config;
                        }
                    }
                }
            }
        }

        // Return result
        return $result;
    }



    /**
     * Get module attribute rule configurations array,
     * from specified index array of entity module objects.
     *
     * Check attribute access callable format:
     * boolean function(DefaultEntityModule $objEntityModule, string $strKey, EntityInterface $objEntity = null).
     *
     * @param array $tabEntityModule
     * @param ValidatorConfigEntity $objEntity = null
     * @param callable $callCheckAttributeAccess = null
     * @param boolean $boolOverwrite = true
     * @return array
     */
    public static function getTabAttributeRuleConfig(
        array $tabEntityModule,
        ValidatorConfigEntity $objEntity = null,
        $callCheckAttributeAccess = null,
        $boolOverwrite = true
    )
    {
        // Init var
        $result = array();
        $tabEntityModule = array_values($tabEntityModule);
        $callCheckAttributeAccess = (is_callable($callCheckAttributeAccess) ? $callCheckAttributeAccess : null);
        $boolOverwrite = (is_bool($boolOverwrite) ? $boolOverwrite : true);

        // Run each entity module
        foreach($tabEntityModule as $objEntityModule)
        {
            // Check entity module eligible
            if($objEntityModule instanceof ValidatorEntityModule)
            {
                // Get index array of module attribute rule configuration
                $tabRuleConfig = $objEntityModule->getTabAttributeRuleConfig($objEntity);

                // Run each module attribute rule configuration
                foreach($tabRuleConfig as $strAttributeKey => $ruleConfig)
                {
                    // Register module attribute rule configuration, if required
                    if(
                        $objEntityModule->checkAttributeExists($strAttributeKey) &&
                        (
                            (
                                (!is_null($callCheckAttributeAccess)) &&
                                $callCheckAttributeAccess($objEntityModule, $strAttributeKey, $objEntity)
                            ) ||
                            (
                                is_null($callCheckAttributeAccess) &&
                                $objEntityModule->checkAttributeAccess($strAttributeKey, $objEntity)
                            )
                        ) &&
                        (
                            (!array_key_exists($strAttributeKey, $result)) ||
                            $boolOverwrite
                        )
                    )
                    {
                        $result[$strAttributeKey] = $ruleConfig;
                    }
                }

            }
        }

        // Return result
        return $result;
    }



    /**
     * Get specified module attribute formatted value when get action required,
     * from specified index array of entity module objects.
     *
     * Check attribute access callable format:
     * boolean function(DefaultEntityModule $objEntityModule, string $strKey, EntityInterface $objEntity = null).
     *
     * @param array $tabEntityModule
     * @param string $strKey
     * @param mixed $value
     * @param ConfigEntity $objEntity = null
     * @param callable $callCheckAttributeAccess = null
     * @param boolean $boolOverwrite = true
     * @return mixed
     */
    public static function getAttributeValueFormatGet(
        array $tabEntityModule,
        $strKey,
        $value,
        ConfigEntity $objEntity = null,
        $callCheckAttributeAccess = null,
        $boolOverwrite = true
    )
    {
        // Init var
        $result = $value;
        $tabEntityModule = array_values($tabEntityModule);
        $callCheckAttributeAccess = (is_callable($callCheckAttributeAccess) ? $callCheckAttributeAccess : null);
        $boolOverwrite = (is_bool($boolOverwrite) ? $boolOverwrite : true);

        // Run each entity module
        $boolContinue = true;
        for($intCpt = 0; ($intCpt < count($tabEntityModule)) && $boolContinue; $intCpt++)
        {
            // Get info
            $objEntityModule = $tabEntityModule[$intCpt];

            // Check entity module eligible
            if (
                ($objEntityModule instanceof DefaultEntityModule) &&
                $objEntityModule->checkAttributeExists($strKey) &&
                (
                    (
                        (!is_null($callCheckAttributeAccess)) &&
                        $callCheckAttributeAccess($objEntityModule, $strKey, $objEntity)
                    ) ||
                    (
                        is_null($callCheckAttributeAccess) &&
                        $objEntityModule->checkAttributeAccess($strKey, $objEntity)
                    )
                )
            )
            {
                // Get attribute formatted value
                $result = $objEntityModule->getAttributeValueFormatGet($strKey, $value, $objEntity);
                $boolContinue = $boolOverwrite;
            }
        }

        // Return result
        return $result;
    }



    /**
     * Get specified module attribute formatted value when set action required,
     * from specified index array of entity module objects.
     *
     * Check attribute access callable format:
     * boolean function(DefaultEntityModule $objEntityModule, string $strKey, EntityInterface $objEntity = null).
     *
     * @param array $tabEntityModule
     * @param string $strKey
     * @param mixed $value
     * @param ConfigEntity $objEntity = null
     * @param callable $callCheckAttributeAccess = null
     * @param boolean $boolOverwrite = true
     * @return mixed
     */
    public static function getAttributeValueFormatSet(
        array $tabEntityModule,
        $strKey,
        $value,
        ConfigEntity $objEntity = null,
        $callCheckAttributeAccess = null,
        $boolOverwrite = true
    )
    {
        // Init var
        $result = $value;
        $tabEntityModule = array_values($tabEntityModule);
        $callCheckAttributeAccess = (is_callable($callCheckAttributeAccess) ? $callCheckAttributeAccess : null);
        $boolOverwrite = (is_bool($boolOverwrite) ? $boolOverwrite : true);

        // Run each entity module
        $boolContinue = true;
        for($intCpt = 0; ($intCpt < count($tabEntityModule)) && $boolContinue; $intCpt++)
        {
            // Get info
            $objEntityModule = $tabEntityModule[$intCpt];

            // Check entity module eligible
            if (
                ($objEntityModule instanceof DefaultEntityModule) &&
                $objEntityModule->checkAttributeExists($strKey) &&
                (
                    (
                        (!is_null($callCheckAttributeAccess)) &&
                        $callCheckAttributeAccess($objEntityModule, $strKey, $objEntity)
                    ) ||
                    (
                        is_null($callCheckAttributeAccess) &&
                        $objEntityModule->checkAttributeAccess($strKey, $objEntity)
                    )
                )
            )
            {
                // Get attribute formatted value
                $result = $objEntityModule->getAttributeValueFormatSet($strKey, $value, $objEntity);
                $boolContinue = $boolOverwrite;
            }
        }

        // Return result
        return $result;
    }



    /**
     * Get specified module attribute formatted value when get action required, to be saved,
     * from specified index array of entity module objects.
     *
     * Check attribute access callable format:
     * boolean function(DefaultEntityModule $objEntityModule, string $strKey, EntityInterface $objEntity = null).
     *
     * @param array $tabEntityModule
     * @param string $strKey
     * @param mixed $value
     * @param SaveConfigEntity $objEntity = null
     * @param callable $callCheckAttributeAccess = null
     * @param boolean $boolOverwrite = true
     * @return mixed
     */
    public static function getAttributeValueSaveFormatGet(
        array $tabEntityModule,
        $strKey,
        $value,
        SaveConfigEntity $objEntity = null,
        $callCheckAttributeAccess = null,
        $boolOverwrite = true
    )
    {
        // Init var
        $result = $value;
        $tabEntityModule = array_values($tabEntityModule);
        $callCheckAttributeAccess = (is_callable($callCheckAttributeAccess) ? $callCheckAttributeAccess : null);
        $boolOverwrite = (is_bool($boolOverwrite) ? $boolOverwrite : true);

        // Run each entity module
        $boolContinue = true;
        for($intCpt = 0; ($intCpt < count($tabEntityModule)) && $boolContinue; $intCpt++)
        {
            // Get info
            $objEntityModule = $tabEntityModule[$intCpt];

            // Check entity module eligible
            if (
                ($objEntityModule instanceof SaveEntityModule) &&
                $objEntityModule->checkAttributeExists($strKey) &&
                (
                    (
                        (!is_null($callCheckAttributeAccess)) &&
                        $callCheckAttributeAccess($objEntityModule, $strKey, $objEntity)
                    ) ||
                    (
                        is_null($callCheckAttributeAccess) &&
                        $objEntityModule->checkAttributeAccess($strKey, $objEntity)
                    )
                )
            )
            {
                // Get attribute formatted value
                $result = $objEntityModule->getAttributeValueSaveFormatGet($strKey, $value, $objEntity);
                $boolContinue = $boolOverwrite;
            }
        }

        // Return result
        return $result;
    }



    /**
     * Get specified module attribute formatted value when set action required, to be load,
     * from specified index array of entity module objects.
     *
     * Check attribute access callable format:
     * boolean function(DefaultEntityModule $objEntityModule, string $strKey, EntityInterface $objEntity = null).
     *
     * @param array $tabEntityModule
     * @param string $strKey
     * @param mixed $value
     * @param SaveConfigEntity $objEntity = null
     * @param callable $callCheckAttributeAccess = null
     * @param boolean $boolOverwrite = true
     * @return mixed
     */
    public static function getAttributeValueSaveFormatSet(
        array $tabEntityModule,
        $strKey,
        $value,
        SaveConfigEntity $objEntity = null,
        $callCheckAttributeAccess = null,
        $boolOverwrite = true
    )
    {
        // Init var
        $result = $value;
        $tabEntityModule = array_values($tabEntityModule);
        $callCheckAttributeAccess = (is_callable($callCheckAttributeAccess) ? $callCheckAttributeAccess : null);
        $boolOverwrite = (is_bool($boolOverwrite) ? $boolOverwrite : true);

        // Run each entity module
        $boolContinue = true;
        for($intCpt = 0; ($intCpt < count($tabEntityModule)) && $boolContinue; $intCpt++)
        {
            // Get info
            $objEntityModule = $tabEntityModule[$intCpt];

            // Check entity module eligible
            if (
                ($objEntityModule instanceof SaveEntityModule) &&
                $objEntityModule->checkAttributeExists($strKey) &&
                (
                    (
                        (!is_null($callCheckAttributeAccess)) &&
                        $callCheckAttributeAccess($objEntityModule, $strKey, $objEntity)
                    ) ||
                    (
                        is_null($callCheckAttributeAccess) &&
                        $objEntityModule->checkAttributeAccess($strKey, $objEntity)
                    )
                )
            )
            {
                // Get attribute formatted value
                $result = $objEntityModule->getAttributeValueSaveFormatSet($strKey, $value, $objEntity);
                $boolContinue = $boolOverwrite;
            }
        }

        // Return result
        return $result;
    }



}
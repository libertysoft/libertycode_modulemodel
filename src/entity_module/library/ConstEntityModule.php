<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\module_model\entity_module\library;



class ConstEntityModule
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Configuration
    const CONF_DEFAULT_ENTITY_MODULE_KEY_PATTERN_DECORATION = 'entity_module_%1$s';

    // Collection configuration keys
    const TAB_COLLECTION_CONFIG_KEY_CACHE_REQUIRE = 'cache_require';
    const TAB_COLLECTION_CONFIG_KEY_OVERWRITE_REQUIRE = 'overwrite_require';

    // Collection cache configuration keys
    const COLLECTION_CACHE_KEY_ATTRIBUTE_CONFIG = 'attribute-config';
    const COLLECTION_CACHE_KEY_ATTRIBUTE_RULE_CONFIG = 'attribute-rule-config';
    const COLLECTION_CACHE_KEY_ATTRIBUTE_ACCESS = 'attribute-access';



	// Exception message constants
    const EXCEPT_MSG_KEY_INVALID_FORMAT = 'Following key "%1$s" invalid! The key must be a string, not empty.';
    const EXCEPT_MSG_COLLECTION_CONFIG_INVALID_FORMAT =
        'Following config "%1$s" invalid! 
        The config must be an array and following the default entity module collection configuration standard.';
    const EXCEPT_MSG_COLLECTION_KEY_INVALID_FORMAT = 'Key invalid! The key "%1$s" must be a valid string in collection.';
    const EXCEPT_MSG_COLLECTION_VALUE_INVALID_FORMAT = 'Value invalid! The value "%1$s" must be a valid entity module object in collection.';
}
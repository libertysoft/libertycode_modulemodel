<?php

namespace liberty_code\module_model\entity_module\test;

use liberty_code\handle_model\attribute\provider\standard\model\StandardAttrProvider;

use liberty_code\handle_model\attribute\repository\model\DefaultSaveAttributeCollection;
use liberty_code\handle_model\attribute\build\model\DefaultBuilder;



/**
 * @method DefaultSaveAttributeCollection getObjAttributeCollection() @inheritdoc
 * @method void setObjAttributeCollection(DefaultSaveAttributeCollection $objAttributeCollection) @inheritdoc
 */
class ParamAttrProvider extends StandardAttrProvider
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /**
     * Init instances table to dissociate this class from parent
     * @var array
     */
    static protected $__instanceTab = array();



    /**
     * DI: Attribute builder instance.
     * @var DefaultBuilder
     */
    protected $objAttributeBuilder;





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param DefaultSaveAttributeCollection $objAttributeCollection
     * @param DefaultBuilder $objAttributeBuilder
     */
    public function __construct(
        DefaultSaveAttributeCollection $objAttributeCollection,
        DefaultBuilder $objAttributeBuilder
    )
    {
        // Init properties
        $this->objAttributeBuilder = $objAttributeBuilder;

        // Call parent constructor
        parent::__construct($objAttributeCollection);

        // Hydrate attribute collection
        $this->hydrateAttributeCollection();
    }





    // Methods initialize
    // ******************************************************************************

    /**
     * Hydrate attribute collection object.
     */
    public function hydrateAttributeCollection()
    {
        // Init var
        $objAttributeCollection =  $this->getObjAttributeCollection();

        // Hydrate attribute collection
        $tabDataSrc = array(
            'attr_1' => [
                'attribute_config' => [
                    'name' => 'Attr 1',
                    'alias' => 'attr-1',
                    'default_value' => 'text 1',
                ],
                'data_type' => 'string'
            ],
            'attr_2' => [
                'type' => 'save',
                'attribute_config' => [
                    'name' => 'Attr 2',
                    'alias' => 'attr-2',
                    'default_value' => 5,
                ],
                'data_type' => 'integer',
                'rule_config' => [
                    [
                        'compare_between',
                        [
                            'greater_compare_value' => 5,
                            'less_compare_value' => 10,
                        ]
                    ]
                ]
            ],
            'attr_3' => [
                'type' => 'save',
                'attribute_config' => [
                    'name' => 'Attr 3',
                    'alias' => 'attr-3',
                    'default_value' => true,
                ],
                'data_type' => 'boolean'
            ]
        );
        $this->objAttributeBuilder->setTabDataSrc($tabDataSrc);
        $this->objAttributeBuilder->hydrateAttributeCollection($objAttributeCollection);
    }





    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getStrFixAttributeCollectionClassPath()
    {
        // Return result
        return DefaultSaveAttributeCollection::class;
    }



}
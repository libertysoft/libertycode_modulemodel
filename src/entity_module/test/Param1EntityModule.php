<?php

namespace liberty_code\module_model\entity_module\test;

use liberty_code\module_model\entity_module\model\SaveEntityModule;

use liberty_code\model\entity\library\ConstEntity;
use liberty_code\model\entity\model\ConfigEntity;
use liberty_code\model\entity\repository\library\ConstSaveEntity;
use liberty_code\model\entity\repository\model\SaveConfigEntity;



class Param1EntityModule extends SaveEntityModule
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /**
     * Init instances table to dissociate this class from parent
     * @var array
     */
    static protected $__instanceTab = array();





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Methods overwrite
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function checkAttributeValueValid(
        $strKey,
        $value,
        array &$tabError = array(),
        ConfigEntity $objEntity = null
    )
    {
        // Init var
        $result = false;
        $tabError = array();

        // Check by attribute
        switch($strKey)
        {
            case 'param_1':
                $result = (is_string($value) && (trim($value) != ''));

                if(!$result)
                {
                    $tabError[] = sprintf(
                        'Following %1$s "%2$s" invalid! The %1$s must be a valid string.',
                        $strKey,
                        mb_strimwidth(strval($value), 0, 50, "...")
                    );
                }

                break;

            case 'param_2':
                $result = is_integer($value);

                if(!$result)
                {
                    $tabError[] = sprintf(
                        'Following %1$s "%2$s" invalid! The %1$s must be an integer.',
                        $strKey,
                        mb_strimwidth(strval($value), 0, 50, "...")
                    );
                }

                break;

            case 'param_3':
                $result = is_bool($value);

                if(!$result)
                {
                    $tabError[] = sprintf(
                        'Following %1$s "%2$s" invalid! The %1$s must be a boolean.',
                        $strKey,
                        mb_strimwidth(strval($value), 0, 50, "...")
                    );
                }

                break;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function checkAttributeAccess($strKey, ConfigEntity $objEntity = null)
    {
        // Init var
        $result =
            parent::checkAttributeAccess($strKey, $objEntity) &&
            ($strKey != 'param_1');

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function getTabAttributeConfig()
    {
        return array(
            // Attribute 1
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY => 'param_1',
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE => 'prm_1',
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => 'Value 1'
            ],

            // Attribute 2
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY => 'param_2',
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE => 'prm_2',
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => 0
            ],

            // Attribute 3
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY => 'param_3',
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE => 'prm_3',
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => false
            ]
        );
    }



    /**
     * @inheritdoc
     */
    public function getAttributeValueFormatGet($strKey, $value, ConfigEntity $objEntity = null)
    {
        // Init var
        $result = parent::getAttributeValueFormatGet($strKey, $value);

        // Format by attribute
        switch($strKey)
        {
            case 'param_1':
                $result = (is_string($value) ? strtoupper($value): $value);
                break;

            case 'param_3':
                $result = (is_bool($value) ? ($value ? 1 : 0) : $value);
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function getAttributeValueFormatSet($strKey, $value, ConfigEntity $objEntity = null)
    {
        // Init var
        $result = parent::getAttributeValueFormatSet($strKey, $value, $objEntity);

        // Format by attribute
        switch($strKey)
        {
            case 'param_1':
                $result = (is_string($value) ? strtolower($value): $value);
                break;

            case 'param_3':
                $result = (
                    // Case boolean
                    is_bool($value) ? $value : (
                        // Case numeric
                        is_numeric($value) ? intval($value) != 0 : (
                            // Case else: format impossible
                            $value
                        )
                    )
                );

                break;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function getAttributeValueSaveFormatGet($strKey, $value, SaveConfigEntity $objEntity = null)
    {
        // Format by attribute
        switch($strKey)
        {
            case 'param_3':
                $result = (is_bool($value) ? ($value ? 'TR' : 'FL') : $value);
                break;

            default:
                $result = $value;
                break;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function getAttributeValueSaveFormatSet($strKey, $value, SaveConfigEntity $objEntity = null)
    {
        // Format by attribute
        switch($strKey)
        {
            case 'param_3':
                $result = (($value == 'TR') ? true : (($value == 'FL') ? false : $value));
                break;

            default:
                $result = $value;
                break;
        }

        // Return result
        return $result;
    }



}
<?php

namespace liberty_code\module_model\entity_module\test;

use liberty_code\module_model\entity_module\build\fix\model\FixBuilder;

use liberty_code\module_model\entity_module\test\Param1EntityModule;
use liberty_code\module_model\entity_module\test\Param2EntityModule;
use liberty_code\module_model\entity_module\test\Param3EntityModule;
use liberty_code\module_model\entity_module\test\Param4EntityModule;



class ParamEntityModuleBuilder extends FixBuilder
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /**
     * Init instances table to dissociate this class from parent
     * @var array
     */
    static protected $__instanceTab = array();





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getTabFixDataSrc()
    {
        // Return result
        return array(
            Param1EntityModule::class,
            Param2EntityModule::class,
            Param3EntityModule::class,
            Param4EntityModule::class
        );
    }



}
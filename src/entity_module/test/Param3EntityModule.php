<?php

namespace liberty_code\module_model\entity_module\test;

use liberty_code\module_model\entity_module\model\SaveEntityModule;

use liberty_code\model\entity\library\ConstEntity;
use liberty_code\model\entity\model\ConfigEntity;
use liberty_code\model\entity\model\ValidatorConfigEntity;
use liberty_code\model\entity\repository\library\ConstSaveEntity;
use liberty_code\model\entity\repository\model\SaveConfigEntity;



class Param3EntityModule extends SaveEntityModule
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /**
     * Init instances table to dissociate this class from parent
     * @var array
     */
    static protected $__instanceTab = array();





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Methods overwrite
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function getTabAttributeConfig()
    {
        return array(
            // Attribute 7
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY => 'param_7',
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE => 'prm_7',
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => 'Value 7'
            ],

            // Attribute 8
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY => 'param_8',
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE => 'prm_8',
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => 7
            ],

            // Attribute 9
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY => 'param_9',
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE => 'prm_9',
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => false
            ]
        );
    }



    /**
     * @inheritdoc
     */
    public function getTabAttributeRuleConfig(ValidatorConfigEntity $objEntity = null)
    {
        // Init var
        $result = array(
            'param_7' => [
                [
                    'type_string',
                    [
                        'sub_rule_not',
                        [
                            'rule_config' => ['is_empty'],
                            'error_message_pattern' => '%1$s is empty.'
                        ]
                    ]
                ]
            ],
            'param_8' => [
                [
                    'type_numeric',
                    ['integer_only_require' => 1]
                ]
            ],
            'param_9' => [
                [
                    'type_boolean',
                    [
                        'integer_enable_require' => 0,
                        'string_enable_require' => 0
                    ]
                ]
            ]
        );

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function getAttributeValueFormatGet($strKey, $value, ConfigEntity $objEntity = null)
    {
        // Init var
        $result = parent::getAttributeValueFormatGet($strKey, $value, $objEntity);

        // Format by attribute
        switch($strKey)
        {
            case 'param_7':
                $result = (is_string($value) ? strtoupper($value): $value);
                break;

            case 'param_9':
                $result = (is_bool($value) ? ($value ? 1 : 0) : $value);
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function getAttributeValueFormatSet($strKey, $value, ConfigEntity $objEntity = null)
    {
        // Init var
        $result = parent::getAttributeValueFormatSet($strKey, $value, $objEntity);

        // Format by attribute
        switch($strKey)
        {
            case 'param_7':
                $result = (is_string($value) ? strtolower($value): $value);
                break;

            case 'param_9':
                $result = (
                    // Case boolean
                    is_bool($value) ? $value : (
                        // Case numeric
                        is_numeric($value) ? intval($value) != 0 : (
                            // Case else: format impossible
                            $value
                        )
                    )
                );

                break;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function getAttributeValueSaveFormatGet($strKey, $value, SaveConfigEntity $objEntity = null)
    {
        // Format by attribute
        switch($strKey)
        {
            case 'param_9':
                $result = (is_bool($value) ? ($value ? 'TR' : 'FL') : $value);
                break;

            default:
                $result = $value;
                break;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function getAttributeValueSaveFormatSet($strKey, $value, SaveConfigEntity $objEntity = null)
    {
        // Format by attribute
        switch($strKey)
        {
            case 'param_9':
                $result = (($value == 'TR') ? true : (($value == 'FL') ? false : $value));
                break;

            default:
                $result = $value;
                break;
        }

        // Return result
        return $result;
    }



}
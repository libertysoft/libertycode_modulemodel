<?php

// Init var
$strEntityModuleRootAppPath = dirname(__FILE__) . '/../../..';

// Load external library
require_once($strEntityModuleRootAppPath . '/vendor/autoload.php'); // Composer vendor

// Load library
require_once($strEntityModuleRootAppPath . '/include/Include.php');

// Load external library test
require_once($strEntityModuleRootAppPath . '/vendor/liberty_code/validation/test/validator/boot/ValidatorBootstrap.php');

// Load test
require($strEntityModuleRootAppPath . '/vendor/liberty_code/handle_model/test/attribute/specification/type/build/boot/DataTypeBuilderBootstrap.php');
require($strEntityModuleRootAppPath . '/vendor/liberty_code/handle_model/test/attribute/specification/boot/AttrSpecBootstrap.php');
require_once($strEntityModuleRootAppPath . '/src/entity_module/test/Param1EntityModule.php');
require_once($strEntityModuleRootAppPath . '/src/entity_module/test/Param2EntityModule.php');
require_once($strEntityModuleRootAppPath . '/src/entity_module/test/Param3EntityModule.php');
require_once($strEntityModuleRootAppPath . '/src/entity_module/test/ParamAttrProvider.php');
require_once($strEntityModuleRootAppPath . '/src/entity_module/test/Param4EntityModule.php');
require_once($strEntityModuleRootAppPath . '/src/entity_module/test/ParamEntityModuleBuilder.php');

// Use
use liberty_code\di\dependency\model\DefaultDependencyCollection;
use liberty_code\di\dependency\preference\model\Preference;
use liberty_code\di\provider\model\DefaultProvider;
use liberty_code\handle_model\attribute\repository\model\DefaultSaveAttributeCollection;
use liberty_code\handle_model\attribute\factory\standard\model\StandardAttributeFactory;
use liberty_code\handle_model\attribute\build\model\DefaultBuilder;
use liberty_code\module_model\entity_module\test\ParamAttrProvider;
use liberty_code\module_model\entity_module\test\ParamEntityModuleBuilder;



// Init DI
$objDepCollection = new DefaultDependencyCollection();
$objProvider = new DefaultProvider($objDepCollection);



// Init data type collection
$tabDataSrc = array(
    [
        'type' => 'string',
        'config' => [
            'type' => 'string'
        ]
    ],
    [
        'type' => 'numeric',
        'config' => [
            'type' => 'numeric'
        ]
    ],
    [
        'type' => 'numeric',
        'config' => [
            'type' => 'integer',
            'integer_require' => true,
            'greater_compare_value' => 0
        ]
    ],
    [
        'type' => 'boolean',
        'config' => [
            'type' => 'boolean'
        ]
    ],
    [
        'type' => 'date',
        'config' => [
            'type' => 'date'
        ]
    ]
);
$objDataTypeBuilder->setTabDataSrc($tabDataSrc);
$objDataTypeBuilder->hydrateDataTypeCollection($objDataTypeCollection, true);



// Init attribute provider
$objAttributeFactory = new StandardAttributeFactory(
    null,
    null,
    $objAttrSpec
);
$objAttributeBuilder = new DefaultBuilder($objAttributeFactory);
$objAttributeCollection = new DefaultSaveAttributeCollection();
$objAttrProvider = new ParamAttrProvider(
    $objAttributeCollection,
    $objAttributeBuilder
);

$objPref = new Preference(array(
    'source' => ParamAttrProvider::class,
    'set' =>  ['type' => 'instance', 'value' => $objAttrProvider],
    'option' => [
        'shared' => true
    ]
));
$objProvider->getObjDependencyCollection()->setDependency($objPref);



// Init entity module builder
$objEntityModuleBuilder = new ParamEntityModuleBuilder($objProvider);



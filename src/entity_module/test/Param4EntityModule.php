<?php

namespace liberty_code\module_model\entity_module\test;

use liberty_code\module_model\entity_module\handle\model\HandleSaveEntityModule;

use liberty_code\handle_model\attribute\provider\api\AttrProviderInterface;
use liberty_code\module_model\entity_module\test\ParamAttrProvider;



/**
 * @method ParamAttrProvider getObjAttrProvider() @inheritdoc
 */
class Param4EntityModule extends HandleSaveEntityModule
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /**
     * Init instances table to dissociate this class from parent
     * @var array
     */
    static protected $__instanceTab = array();





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param ParamAttrProvider $objAttrProvider
     */
    public function __construct(
        ParamAttrProvider $objAttrProvider,
        $tabData = array()
    )
    {
        // Call parent constructor
        parent::__construct($objAttrProvider, $tabData);
    }





    // Methods setters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function setAttrProvider(AttrProviderInterface $objAttrProvider)
    {
        // Set data, if required
        if($objAttrProvider instanceof ParamAttrProvider)
        {
            // Call parent method
            parent::setAttrProvider($objAttrProvider);
        }
    }



}
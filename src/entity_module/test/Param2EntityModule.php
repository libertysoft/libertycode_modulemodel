<?php

namespace liberty_code\module_model\entity_module\test;

use liberty_code\module_model\entity_module\model\SaveEntityModule;

use liberty_code\model\entity\library\ConstEntity;
use liberty_code\model\entity\model\ConfigEntity;
use liberty_code\model\entity\model\ValidatorConfigEntity;
use liberty_code\model\entity\repository\library\ConstSaveEntity;
use liberty_code\model\entity\repository\model\SaveConfigEntity;



class Param2EntityModule extends SaveEntityModule
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /**
     * Init instances table to dissociate this class from parent
     * @var array
     */
    static protected $__instanceTab = array();





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Methods overwrite
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function getTabAttributeConfig()
    {
        return array(
            // Attribute 4
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY => 'param_4',
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE => 'prm_4',
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => 'Value 4'
            ],

            // Attribute 5
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY => 'param_5',
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE => 'prm_5',
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => 5
            ],

            // Attribute 6
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY => 'param_6',
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE => 'prm_6',
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => true
            ]
        );
    }



    /**
     * @inheritdoc
     */
    public function getTabAttributeRuleConfig(ValidatorConfigEntity $objEntity = null)
    {
        // Init var
        $result = array(
            'param_4' => [
                [
                    'type_string',
                    [
                        'sub_rule_not',
                        [
                            'rule_config' => ['is_empty'],
                            'error_message_pattern' => '%1$s is empty.'
                        ]
                    ]
                ]
            ],
            'param_5' => [
                [
                    'type_numeric',
                    ['integer_only_require' => 1]
                ]
            ],
            'param_6' => [
                [
                    'type_boolean',
                    [
                        'integer_enable_require' => 0,
                        'string_enable_require' => 0
                    ]
                ]
            ]
        );

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function getAttributeValueFormatGet($strKey, $value, ConfigEntity $objEntity = null)
    {
        // Init var
        $result = parent::getAttributeValueFormatGet($strKey, $value, $objEntity);

        // Format by attribute
        switch($strKey)
        {
            case 'param_4':
                $result = (is_string($value) ? strtoupper($value): $value);
                break;

            case 'param_6':
                $result = (is_bool($value) ? ($value ? 1 : 0) : $value);
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function getAttributeValueFormatSet($strKey, $value, ConfigEntity $objEntity = null)
    {
        // Init var
        $result = parent::getAttributeValueFormatSet($strKey, $value, $objEntity);

        // Format by attribute
        switch($strKey)
        {
            case 'param_4':
                $result = (is_string($value) ? strtolower($value): $value);
                break;

            case 'param_6':
                $result = (
                    // Case boolean
                    is_bool($value) ? $value : (
                        // Case numeric
                        is_numeric($value) ? intval($value) != 0 : (
                            // Case else: format impossible
                            $value
                        )
                    )
                );

                break;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function getAttributeValueSaveFormatGet($strKey, $value, SaveConfigEntity $objEntity = null)
    {
        // Format by attribute
        switch($strKey)
        {
            case 'param_6':
                $result = (is_bool($value) ? ($value ? 'TR' : 'FL') : $value);
                break;

            default:
                $result = $value;
                break;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function getAttributeValueSaveFormatSet($strKey, $value, SaveConfigEntity $objEntity = null)
    {
        // Format by attribute
        switch($strKey)
        {
            case 'param_6':
                $result = (($value == 'TR') ? true : (($value == 'FL') ? false : $value));
                break;

            default:
                $result = $value;
                break;
        }

        // Return result
        return $result;
    }



}
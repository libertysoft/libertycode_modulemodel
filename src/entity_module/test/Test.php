<?php

// Init var
$strTestRootAppPath = dirname(__FILE__) . '/../../..';

// Load test
require_once($strTestRootAppPath . '/src/entity_module/test/EntityModuleBuilderTest.php');
require_once($strTestRootAppPath . '/src/entity_module/test/ParamEntity.php');

// Use
use liberty_code\module_model\entity_module\model\DefaultEntityModuleCollection;
use liberty_code\module_model\entity_module\test\ParamEntity;



// Init var
$objParamEntity = new ParamEntity(
    array(),
    $objValidator
);

$objEntityModuleCollection = new DefaultEntityModuleCollection();
$objEntityModuleCollection->setConfig(array(
    //'cache_require' => 0
));
$objEntityModuleBuilder->hydrateEntityModuleCollection($objEntityModuleCollection);
$objEntityModuleCollection->setEntity($objParamEntity);



// Test entity module
$tabKey = $objEntityModuleCollection->beanGetTabData();
echo('Entity module keys: <pre>');print_r($tabKey);echo('</pre>');

echo('<br /><br /><br />');



// Test configuration
$tabAttributeConfig = $objEntityModuleCollection->getTabAttributeConfig();
echo('Attribute configuration: <pre>');print_r($tabAttributeConfig);echo('</pre>');

echo('<br /><br /><br />');



// Test rule configuration
$tabAttributeRuleConfig = $objEntityModuleCollection->getTabAttributeRuleConfig();
echo('Attribute rule configuration: <pre>');var_dump($tabAttributeRuleConfig);echo('</pre>');

echo('<br /><br /><br />');



// Test check/get attribute
$tabKey = array(
    'param_1' => 'Test 1', // Found, valid
    'param_2' => '', // Found, not valid
    'param_3' => 'test', // Found, not valid
    'param_4' => 'Test 4', // Found
    'param_5' => 7, // Found
    'param_6' => false, // Found
    'param_7' => 'Test 7', // Found
    'param_8' => 9, // Found
    'param_9' => true, // Found
    'param_10' => false, // Not found
    10 => false // Ko: Key in valid
);

foreach($tabKey as $strKey => $value)
{
    echo('Test check, get attribute "'.strval($strKey).'": value "'.var_export($value, true).'":<br />');

    try{
        $objParamEntity->setAttributeValue($strKey, $value);
        $entityValue = $objParamEntity->getAttributeValue($strKey, false);

        $tabError = array();
        echo('Check attribute: <pre>');var_dump($objEntityModuleCollection->checkAttributeValueValid($strKey, $entityValue, $tabError));echo('</pre>');
        echo('Get attribute error: <pre>');var_dump($tabError);echo('</pre>');
        echo('Get attribute formatted value get: <pre>');var_dump($objEntityModuleCollection->getAttributeValueFormatGet($strKey, $value));echo('</pre>');
        echo('Get attribute formatted value set: <pre>');var_dump($objEntityModuleCollection->getAttributeValueFormatSet($strKey, $value));echo('</pre>');
        echo('Get attribute formatted value saved get: <pre>');var_dump($objEntityModuleCollection->getAttributeValueSaveFormatGet($strKey, $value));echo('</pre>');
        echo('Get attribute formatted value saved set: <pre>');var_dump($objEntityModuleCollection->getAttributeValueSaveFormatSet($strKey, $value));echo('</pre>');

    } catch(\Exception $e) {
        echo(htmlentities(get_class($e) . '/' . $e->getMessage()));
        echo('<br />');
    }
    echo('<br /><br /><br />');
}

echo('<br /><br /><br />');



// Test remove collection entity module
$objEntityModuleCollection->removeEntityModuleAll();
echo('Test remove item: <pre>');var_dump($objEntityModuleCollection->beanGetTabData());echo('</pre>');

echo('<br /><br /><br />');



<?php
/**
 * Description :
 * This class allows to define module validator configured entity class.
 * Module validator configured entity is validator configured entity, using entity module collection,
 * to manage its attributes.
 *
 * Module configured validator entity allows to configure attributes from following configuration (feature 'getTabConfig'):
 * [
 *     Validator configured entity configuration
 * ]
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code\module_model\entity\model;

use liberty_code\model\entity\model\ValidatorConfigEntity;

use liberty_code\validation\validator\api\ValidatorInterface;
use liberty_code\module_model\entity_module\library\ConstEntityModule;
use liberty_code\module_model\entity_module\model\DefaultEntityModuleCollection;



class ModuleValidatorConfigEntity extends ValidatorConfigEntity
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();



    /**
     * Entity module collection
     * @var DefaultEntityModuleCollection
     */
    protected $objEntityModuleCollection;
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param DefaultEntityModuleCollection $objEntityModuleCollection
     */
    public function __construct(
        DefaultEntityModuleCollection $objEntityModuleCollection,
        array $tabValue = array(),
        ValidatorInterface $objValidator = null
    )
    {
        // Set entity module collection
        $this->setEntityModuleCollection($objEntityModuleCollection);

        // Call parent constructor
        parent::__construct($tabValue, $objValidator);

        // Clear entity module collection cache
        $objEntityModuleCollection->removeCache(ConstEntityModule::COLLECTION_CACHE_KEY_ATTRIBUTE_RULE_CONFIG);
    }





    // Methods getters
    // ******************************************************************************

    /**
     * Get entity module collection object.
     *
     * @return DefaultEntityModuleCollection
     */
    public function getObjEntityModuleCollection()
    {
        // Return result
        return $this->objEntityModuleCollection;
    }



    /**
     * @inheritdoc
     */
    protected function getTabConfig()
    {
        // Return result
        return $this->getObjEntityModuleCollection()->getTabAttributeConfig();
    }



    /**
     * @inheritdoc
     */
    protected function getTabRuleConfig()
    {
        // Return result
        return $this->getObjEntityModuleCollection()->getTabAttributeRuleConfig();
    }



    /**
     * @inheritdoc
     */
    protected function getAttributeValueFormatGet($strKey, $value)
    {
        // Return result
        return $this->getObjEntityModuleCollection()->getAttributeValueFormatGet($strKey, $value);
    }



    /**
     * @inheritdoc
     */
    protected function getAttributeValueFormatSet($strKey, $value)
    {
        // Return result
        return $this->getObjEntityModuleCollection()->getAttributeValueFormatSet($strKey, $value);
    }





    // Methods setters
    // ******************************************************************************

    /**
     * Set entity module collection object.
     *
     * @param DefaultEntityModuleCollection $objEntityModuleCollection
     */
    public function setEntityModuleCollection(DefaultEntityModuleCollection $objEntityModuleCollection)
    {
        // Set data
        $objEntityModuleCollection->setEntity($this);
        $this->objEntityModuleCollection = $objEntityModuleCollection;
    }



}
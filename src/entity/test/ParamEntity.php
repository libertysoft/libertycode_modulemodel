<?php

namespace liberty_code\module_model\entity\test;

use liberty_code\module_model\entity\repository\model\ModuleSaveConfigEntity;

use liberty_code\validation\validator\api\ValidatorInterface;
use liberty_code\module_model\entity_module\model\DefaultEntityModuleCollection;
use liberty_code\module_model\entity_module\test\ParamEntityModuleBuilder;



class ParamEntity extends ModuleSaveConfigEntity
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /**
     * Init instances table to dissociate this class from parent
     * @var array
     */
    static protected $__instanceTab = array();



    /**
     * DI: Param entity module builder instance.
     * @var ParamEntityModuleBuilder
     */
    protected $objEntityModuleBuilder;





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param ParamEntityModuleBuilder $objEntityModuleBuilder
     */
    public function __construct(
        DefaultEntityModuleCollection $objEntityModuleCollection,
        ParamEntityModuleBuilder $objEntityModuleBuilder,
        array $tabValue = array(),
        ValidatorInterface $objValidator = null
    )
    {
        // Init properties
        $this->objEntityModuleBuilder = $objEntityModuleBuilder;

        // Call parent constructor
        parent::__construct($objEntityModuleCollection, $tabValue, $objValidator);
    }





    // Methods setters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function setEntityModuleCollection(DefaultEntityModuleCollection $objEntityModuleCollection)
    {
        // Set data
        parent::setEntityModuleCollection($objEntityModuleCollection);

        // Hydrate entity module collection
        $this->objEntityModuleBuilder->hydrateEntityModuleCollection($objEntityModuleCollection);
    }



}
<?php

// Init var
$strTestRootAppPath = dirname(__FILE__) . '/../../..';

// Load test
require_once($strTestRootAppPath . '/src/entity_module/test/EntityModuleBuilderTest.php');
require_once($strTestRootAppPath . '/src/entity/test/ParamEntity.php');

// Use
use liberty_code\module_model\entity_module\model\DefaultEntityModuleCollection;
use liberty_code\module_model\entity\test\ParamEntity;



// Init var
$objEntityModuleCollection = new DefaultEntityModuleCollection();
$objEntityModuleCollection->setConfig(array(
    //'cache_require' => 0
));

$objParamEntity = new ParamEntity(
    $objEntityModuleCollection,
    $objEntityModuleBuilder,
    array(),
    $objValidator
);



// Test check/get attribute
$tabKey = array(
    'param_1' => 'Test 1', // Not found (attribute access failed)
    'param_2' => 7, // Found
    'param_3' => true, // Found
    'param_4' => 'Test 4', // Found, valid
    'param_5' => '', // Found, not valid
    'param_6' => 'Test 6', // Found, not valid
    'param_7' => 'Test 7', // Found, valid
    'param_8' => 9, // Found, valid
    'param_9' => true, // Found, valid
    'param_10' => false, // Not found
    10 => false // Ko: Key in valid
);

foreach($tabKey as $strKey => $value)
{
    echo('Test check, get attribute "'.strval($strKey).'": value "'.var_export($value, true).'":<br />');

    try{
        $objParamEntity->setAttributeValue($strKey, $value);

        $tabError = array();
        echo('Check attribute: <pre>');var_dump($objParamEntity->checkAttributeValid($strKey, $tabError));echo('</pre>');
        echo('Get attribute error: <pre>');var_dump($tabError);echo('</pre>');
        echo('Get attribute formatted: <pre>');var_dump($objParamEntity->getAttributeValue($strKey));echo('</pre>');
        echo('Get attribute without formatted: <pre>');var_dump($objParamEntity->getAttributeValue($strKey, false));echo('</pre>');

    } catch(\Exception $e) {
        echo(htmlentities(get_class($e) . '/' . $e->getMessage()));
        echo('<br />');
    }
    echo('<br /><br /><br />');
}

echo('<br /><br /><br />');



// Test get attribute value save
echo('Test get attribute value save: <pre>');var_dump($objParamEntity->getTabDataSave());echo('</pre>');

echo('<br /><br /><br />');


